package facci.gabrielarias.pasaje;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Detalle extends AppCompatActivity {
    TextView txtCedula, txtNombres, txtApellidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        txtNombres = findViewById(R.id.txtNombres);
        txtApellidos = findViewById(R.id.txtApellidos);
        txtCedula = findViewById(R.id.txtCedula);


        txtNombres.setText(getIntent().getStringExtra("nombres"));
        txtApellidos.setText(getIntent().getStringExtra("apellidos"));
        txtCedula.setText(getIntent().getStringExtra("cedula"));





    }
}
