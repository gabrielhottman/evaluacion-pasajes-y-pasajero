package facci.gabrielarias.pasaje.database.entidad;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import facci.gabrielarias.pasaje.database.helper.ClaseHelper;
import facci.gabrielarias.pasaje.database.modelo.Pasaje;
import facci.gabrielarias.pasaje.database.modelo.Pasajero;

public class ItemDB {


    ClaseHelper claseHelper;

    public ItemDB(Context context) {
        claseHelper = new ClaseHelper(context);
    }

    /* Clase interna que define el contenido de cada tabla  */
    public static abstract class ElementosDeEntrada implements BaseColumns {

        //TABLA PASAJE
        public static final String TABLE_NAME_PASAJE = "Pasaje";
        public static final String COLUMN_NAME_NUMPASAJE = "numeroDePasaje";
        public static final String COLUMN_NAME_AVION = "avionQueViaja";
        public static final String COLUMN_NAME_FECHA_HORA = "fechaHora";
        public static final String COLUMN_NAME_CEDULA_PASAJERO = "cedulaPasajero";
        public static final String COLUMN_NAME_ORIGEN_DESTINO = "origenDestino";

        public static final String CREATE_TABLE_PASAJE = "CREATE TABLE " +
                TABLE_NAME_PASAJE + " (" +
                "id" + " INTEGER PRIMARY KEY, "  +
                COLUMN_NAME_NUMPASAJE + " TEXT, "  +
                COLUMN_NAME_AVION + " TEXT, " +
                COLUMN_NAME_FECHA_HORA + " TEXT, "  +
                COLUMN_NAME_CEDULA_PASAJERO + " TEXT, "  +
                COLUMN_NAME_ORIGEN_DESTINO + " TEXT  )";
        public static final String DELETE_TABLE_PASAJE = "DROP TABLE IF EXISTS " + TABLE_NAME_PASAJE;

        //TABLA PASAJERO
        public static final String TABLE_NAME_PASAJERO = "Pasajero";
        public static final String COLUMN_NAME_NOMBRES = "nombres";
        public static final String COLUMN_NAME_APELLIDOS = "apellidos";
        public static final String COLUMN_NAME_CEDULA = "cedula";

        public static final String CREATE_TABLE_PASAJERO = "CREATE TABLE " +
                TABLE_NAME_PASAJERO + " (" +
                "ID" + " INTEGER PRIMARY KEY, "  +
                COLUMN_NAME_NOMBRES + " TEXT, " +
                COLUMN_NAME_APELLIDOS + " TEXT, " +
                COLUMN_NAME_CEDULA + " TEXT)";
        public static final String DELETE_TABLE_PASAJERO = "DROP TABLE IF EXISTS " + TABLE_NAME_PASAJERO;

    }

    int c = 0;
    public void insertarPasaje(String numeroPasaje, String avionQueViaja, String fechaHora, String cedulaPasajero, String origenDestino){
        SQLiteDatabase base = claseHelper.getWritableDatabase();
        ContentValues contenido = new ContentValues();

        contenido.put("numeroDePasaje", numeroPasaje);
        contenido.put("avionQueViaja", avionQueViaja);
        contenido.put("fechaHora", fechaHora);
        contenido.put("cedulaPasajero", cedulaPasajero);
        contenido.put("origenDestino", origenDestino);

        base.insert(ElementosDeEntrada.TABLE_NAME_PASAJE, null, contenido);
        claseHelper.getWritableDatabase().close();
        c=c+1;
        Log.e("Insartando en BASEDATOS", "INSERTADO CORRECTAMENTE "+c);
    }

    int d=0;
    public void insertarPasajero(String nombres, String apellidos, String cedula){
        SQLiteDatabase base = claseHelper.getWritableDatabase();
        ContentValues contenidoPasajero = new ContentValues();
        contenidoPasajero.put(ElementosDeEntrada.COLUMN_NAME_NOMBRES, nombres);
        contenidoPasajero.put(ElementosDeEntrada.COLUMN_NAME_APELLIDOS, apellidos);
        contenidoPasajero.put(ElementosDeEntrada.COLUMN_NAME_CEDULA, cedula);

        base.insert(ElementosDeEntrada.TABLE_NAME_PASAJERO, null, contenidoPasajero);
        d=d+1;
        claseHelper.getWritableDatabase().close();
        Log.e("Insartando Pasajeros", "INSERTADO CORRECTAMENTE "+d);
    }


    public void eliminarBases(){

        SQLiteDatabase base = claseHelper.getWritableDatabase();

        base.delete(ElementosDeEntrada.TABLE_NAME_PASAJE, null, null);
        base.delete(ElementosDeEntrada.TABLE_NAME_PASAJERO, null, null);
        base.close();
    }
    public ArrayList<Pasaje> obtenerPasajes(){
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        ArrayList<Pasaje> listaPasajes = new ArrayList<>();

        Cursor fila = base.rawQuery("SELECT * FROM "+ElementosDeEntrada.TABLE_NAME_PASAJE, null);
        if(fila.moveToFirst()){
            Log.e("Datos Cursor", "Si hay Pasajes guardados");
            do {
                String id = fila.getString(fila.getColumnIndex("id"));
                String avion = fila.getString(fila.getColumnIndex("avionQueViaja"));
                String fechaHora = fila.getString(fila.getColumnIndex("fechaHora"));
                String cedulaPasajero = fila.getString(fila.getColumnIndex("cedulaPasajero"));
                String origenDestino = fila.getString(fila.getColumnIndex("origenDestino"));

                Pasaje pasaje = new Pasaje(Integer.parseInt(id), avion, fechaHora, cedulaPasajero, origenDestino);
                listaPasajes.add(pasaje);
                Log.e("Datos: ", "id: "+id+" Avión: "+avion+" FechaHora: "+fechaHora+" Cedula Pasajero: "+cedulaPasajero+ " Origen/Destino: "+ origenDestino);
            }while (fila.moveToNext());
            fila.close();
        }
        else {
            Log.e("NO datos Cursor", "NO hay datos");
        }
        base.close();
        return listaPasajes;
    }


    public Pasaje consultaIndividualPasaje(String cedula){
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        Pasaje pasaje = null;

        Cursor fila = base.rawQuery("Select * From Pasaje where cedulaPasajero = " + cedula, null);
        if(fila.moveToFirst()){
            String id = fila.getString(fila.getColumnIndex("id"));
            //String numeroDePasaje = fila.getString(fila.getColumnIndex("numeroDePasaje"));
            String avin = fila.getString(fila.getColumnIndex("avionQueViaja"));
            String fechaH = fila.getString(fila.getColumnIndex("fechaHora"));
            String cedulaP = fila.getString(fila.getColumnIndex("cedulaPasajero"));
            String origenDest = fila.getString(fila.getColumnIndex("origenDestino"));

             pasaje = new Pasaje(Integer.parseInt(id), avin,fechaH,cedulaP,origenDest);

        }
        else{
            Log.e("NO DATO: ", "Registro no Encontrado");
            return null;
        }
        claseHelper.getReadableDatabase().close();
        base.close();
        return pasaje;

    }
    public Pasajero consultaIndividualPasajero(String cedula){
        Pasajero pasajero = null;
        SQLiteDatabase base = claseHelper.getReadableDatabase();
        Cursor fila = base.rawQuery("select * from "+ElementosDeEntrada.TABLE_NAME_PASAJERO+" where cedula = "+cedula, null);
        if (fila.moveToFirst()){
            String id = fila.getString(0);
            String nombre = fila.getString(1);
            String apeliidos = fila.getString(2);
            String cedulaPasajero = fila.getString(3);

            pasajero = new Pasajero(Integer.parseInt(id), cedula,nombre, apeliidos);
            Log.e("Pasajero", id+"-"+nombre+"-"+apeliidos+"-"+cedulaPasajero);

        }
        else {
            Log.e("No Valido", "Registro no encontrado");
        }
        return pasajero;
    }





}
