package facci.gabrielarias.pasaje.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import facci.gabrielarias.pasaje.database.entidad.ItemDB;

public class ClaseHelper extends SQLiteOpenHelper {



    public static int versionDB = 1;
    public static String nombreDB= "Pasaje.db" ;

    public ClaseHelper(@Nullable Context context) {
        super(context, nombreDB, null, versionDB);
    }

    @Override
    public void onCreate(SQLiteDatabase baseDeDatos) {
    baseDeDatos.execSQL(ItemDB.ElementosDeEntrada.CREATE_TABLE_PASAJE);
    baseDeDatos.execSQL(ItemDB.ElementosDeEntrada.CREATE_TABLE_PASAJERO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
