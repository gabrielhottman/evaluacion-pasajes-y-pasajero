package facci.gabrielarias.pasaje.database.modelo;

public class Pasaje {
    private int idNumeroPasaje;
    private String avionQueViaja;
    private String fechaHoraViaje;
    private String cedulaPasajero;
    private String origenDestino;

    public Pasaje(int idNumeroPasaje, String avionQueViaja, String fechaHoraViaje, String cedulaPasajero, String origenDestino) {
        this.idNumeroPasaje = idNumeroPasaje;
        this.avionQueViaja = avionQueViaja;
        this.fechaHoraViaje = fechaHoraViaje;
        this.cedulaPasajero = cedulaPasajero;
        this.origenDestino = origenDestino;
    }

    public int getIdNumeroPasaje() {
        return idNumeroPasaje;
    }

    public void setIdNumeroPasaje(int idNumeroPasaje) {
        this.idNumeroPasaje = idNumeroPasaje;
    }

    public String getAvionQueViaja() {
        return avionQueViaja;
    }

    public void setAvionQueViaja(String avionQueViaja) {
        this.avionQueViaja = avionQueViaja;
    }

    public String getFechaHoraViaje() {
        return fechaHoraViaje;
    }

    public void setFechaHoraViaje(String fechaHoraViaje) {
        this.fechaHoraViaje = fechaHoraViaje;
    }

    public String getCedulaPasajero() {
        return cedulaPasajero;
    }

    public void setCedulaPasajero(String cedulaPasajero) {
        this.cedulaPasajero = cedulaPasajero;
    }

    public String getOrigenDestino() {
        return origenDestino;
    }

    public void setOrigenDestino(String origenDestino) {
        this.origenDestino = origenDestino;
    }

    @Override
    public String toString() {
        return idNumeroPasaje+"   "+avionQueViaja+"   "+fechaHoraViaje+"   "+cedulaPasajero+"   "+origenDestino;
    }
}
