package facci.gabrielarias.pasaje;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import facci.gabrielarias.pasaje.database.entidad.ItemDB;
import facci.gabrielarias.pasaje.database.modelo.Pasaje;
import facci.gabrielarias.pasaje.database.modelo.Pasajero;

public class MainActivity extends AppCompatActivity {
    Button btnConsulta;
    ListView listView1;
    EditText txtValor;

    ItemDB itemDB = new ItemDB(this);
    ArrayList<Pasaje> listaDePasajes = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnConsulta =  (Button) findViewById(R.id.btnConsulta);
        txtValor = (EditText) findViewById(R.id.txtValor);
        listView1 = (ListView) findViewById(R.id.listViewPasaje);

        //Esta Linea sirve para esconder el teclado virtual cuando se inicia la Aplicación
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

//Iniciamos Eliminando todas las Bases de Datos...      Esto es para que no hayan registros duplicados.
        itemDB.eliminarBases();

        insertarPasajeMain();
        insertarPasajeroMain();
        String rutaBasePasaje = getDatabasePath("Pasaje.db").getAbsolutePath();
        Log.e("Ruta", rutaBasePasaje);

        listaDePasajes.addAll(itemDB.obtenerPasajes());


        final ArrayAdapter<Pasaje> adapter = new ArrayAdapter<>(this, R.layout.listview_item_disenado, listaDePasajes);
        listView1.setAdapter(adapter);

        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String cedula = adapter.getItem(position).getCedulaPasajero();


                Pasajero pasajero = itemDB.consultaIndividualPasajero(cedula);
                if(pasajero != null){
                    Intent i = new Intent(MainActivity.this, Detalle.class);
                    //i.putExtra("id", pasajero.getId());
                    i.putExtra("nombres", pasajero.getNombres());
                    i.putExtra("apellidos", pasajero.getApellidos());
                    i.putExtra("cedula", pasajero.getCedula());

                    startActivity(i);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Registro no encontrado", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Boton de consulta
        btnConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valor = txtValor.getText().toString();

                if(!valor.isEmpty()){
                    consultaPasaje(valor);
                    txtValor.setText("");
                }
                else {
                    Toast.makeText(getApplicationContext(), "Ingrese un valor!!!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void insertarPasajeMain(){
        Log.e("MainInsertar", "Insertando Pasaje desde Main");
        itemDB.insertarPasaje("2020", "AirBus, LAN", "29/07/2020", "1314023993", "ECU - COL");
        itemDB.insertarPasaje("2222", "Boeing, TAME", "10/04/2020", "1234567890", "ECU - CHILE");
        itemDB.insertarPasaje("2233", "FALCON, EMIRATES", "29/07/2020", "2222222222", "ECU - QUATAR");
        itemDB.insertarPasaje("3205", "Boeing, AA", "1/02/2021", "5555555555", "ECU - USA");
    }
    public void insertarPasajeroMain(){
        Log.e("MainInsertar", "Insertando Pasajero desde Main");
        itemDB.insertarPasajero("Gabriel Marcelo","Arias Arteaga", "1314023993");
        itemDB.insertarPasajero("Luis Felipe","Lopez Mujica", "2222222222");
        itemDB.insertarPasajero("Diego Manuel","Loor Mendoza", "1354896478");
        itemDB.insertarPasajero("Jevier Andres","Murillo Hernandez", "9999999999");
        itemDB.insertarPasajero("Lady Ivonne","Delgado Delgado", "1234567890");
    }

    public void consultaPasaje(String cedula){
        Pasaje pasaje = itemDB.consultaIndividualPasaje(cedula);
        if (pasaje != null) {
            Toast.makeText(this, ""+pasaje.getAvionQueViaja()+"    "+pasaje.getOrigenDestino(), Toast.LENGTH_SHORT).show();
            Log.e("Pasaje: ", pasaje.getAvionQueViaja());
        }
        else {
            Toast.makeText(this, "Registro no encontrado", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        itemDB.eliminarBases();
        Toast.makeText(this, "Aplicación Destruida",Toast.LENGTH_LONG).show();
        Log.e("Ciclo", "OnDestroy");
    }
}
